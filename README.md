# Project
## Goal
Interpreter for the esoteric programming language Brainfuck.  
Select input file via command line arguments.

## How to use
The file base.c contains the entire interpreter. Compile it with your favorite compiler (Example gcc: `gcc -o bfinterpret base.c`).  
After that you can call the compiled binary on any file and have it be interpreted as a brainfuck file.  
helloWorld.bf is an example file printing `Hello World!\n` to the console and quitting. To run it, call the compiled interpreter like this: `bfinterpret helloWorld.bf`

## How it works
The interpreter scans the file character by character and acts according to the command that character represents. The commands can be found on [https://de.wikipedia.org/wiki/Brainfuck]. 
mainLoop is the main loop going over a file and interpreting every character. If it encounters the start of a loop it'll enter a subloop via the subLoop function, which then continues the interpretation. This subloop stores the entry position. On the end of a loop it checks if the current cell is 0 and if yes it'll exit. If not, it'll return to the previously stored entry position and starts evaluating from there on again.

## Code golf
All versions of golfing, including a tracker of the character count, can be found in the CodeGolf directory. Spoiler: I reduced it from 3313 down to 970
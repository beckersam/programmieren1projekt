/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//
// Created by Samuel on 2/3/23.
//

/*
 * Commands a la https://de.wikipedia.org/wiki/Brainfuck
 */

#include <stdio.h>
#include <stdlib.h>

#define CELL_COUNT 20

typedef struct {
    char cells[CELL_COUNT];
    int current_cell;
} Storage;

Storage *buildStorage(){
    Storage *new = malloc(sizeof(Storage));
    for (int i = 0; i < CELL_COUNT; ++i) {
        new->cells[i] = 0;
    }
    return new;
}

// Updates the current storage according to the latest command
void interpret(Storage *s, char c) {
    switch (c) {
        case '>':
            // If we were to go over the max number of cells, do nothing instead
            // If we were to go over the max number of cells, do nothing instead
            s->current_cell == CELL_COUNT-1 ? : s->current_cell++;
            return;
        case '<':
            // Same here
            s->current_cell == 0 ? : s->current_cell--;
            return;
        case '+':
            // Ignore wraparound for now. Could add a limiter too later
            s->cells[s->current_cell]++;
            return;
        case '-':
            // Same here
            s->cells[s->current_cell]--;
            return;
        case '.':
            putchar(s->cells[s->current_cell]);
            return;
        default:
            return;
    }
}

int subLoop(FILE *f, Storage *s, int i) {
    char c;
    long start_pos = ftell(f);
    while (1) {
        c = (char)fgetc(f);
        switch (c) {
            case '[':
                // New sub-loop
                if (subLoop(f, s) == EOF) {return EOF;}
                continue;
            case ']':
                if (s->cells[s->current_cell] == 0) {
                    return 0;
                } else {
                    fseek(f, start_pos, SEEK_SET);
                }
                continue;
            case EOF:
                // Hit EOF before end of loop. Escalate
                return i ? EOF : 0;
            default:
                // Interpret char
                interpret(s, c);
                continue;
        }
    }
}

int main(int argc, char* argv[]) {
    // Init all the stuff
    if (argc != 2) {
        fprintf(stderr, "Invalid arguments. Need exactly 1 containing the file to use. Quitting\n");
        exit(EXIT_FAILURE);
    }
    FILE *f = fopen(argv[1], "r");
    if (f == NULL) {
        fprintf(stderr, "File %s doesn't exist. Quitting.\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    Storage *s = buildStorage();

    // Run actual main loop
    if (subLoop(f, s, 0)) {
        fprintf(stderr, "Hit EOF before resolving all loops. Quitting");
        exit(EXIT_FAILURE);
    }

    // Don't forget to close the file before exiting. Storage doesn't matter anymore at this point
    fclose(f);

    exit(EXIT_SUCCESS);
}
